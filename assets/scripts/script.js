// console.log("hello");
// Events:
// load
// scroll
// click
// hover
// type
// this is the user's opportunity to interact with our web pages
//
// Task: Click the button and then change the text from Hello to bye!
//
// Actions:
// 1. Capture the button (Elements that will trigger the event)
// 2. Add an event listener
// 3. Create our function
// 4. Set up the Task

// Step 1: Capture the Element
const btn = document.getElementById('byeBtn'); //byeBtn is the ID from the HTML File

// Step 2: 
// 2. Add an event listener
// addEventListener = method; takes 2 params (1. type of event, 2. Function)

btn.addEventListener('click', function(){
	// Step 4.
	//1. We need to capture the element we want to modify.
	//We want to change H1.
	//When we realized, we already targetted btn.
	//previousElementSibling
btn.previousElementSibling.textContent = "Bye World!";
	//we can do more here
	//next goal -- change the bg to red (bg-warning);
	//we realized, the first div is the one responsible for the background.
	//And we have the background because of the bootstrap class bg-info.
	//for us to change the background, we need to remove FIRST bg-info and then add bg-warning.
	//We realized, the div we want to target is the parent of btn.
	btn.parentElement.classList.remove('bg-info');
	btn.parentElement.classList.add('bg-danger');
	//change the color of the btn.
	//clue: btn-warning is the one responsible for the color

	btn.classList.remove('btn-warning');
	btn.classList.add('btn-info');

	setTimeout(function(){
		btn.previousElementSibling.textContent = "Hello World!";

	}, 2000)


});

