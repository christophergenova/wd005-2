// const wordInput = document.getElementsById('formInput');

// wordInput.addEventListener('keyup', function(){
// 	// we want to get the value of the input and then publish it in p 
// 	//since we already capture inputm we can acces its value via dot notation;
// 	const inputValue = wordInput.value;
// 	// console.log(inputValue);
// 	const p = document.getElementById('words');
// 	p.textContent = inputValue;
// })

const wordInput = document.getElementById('formInput');

wordInput.addEventListener('keyup', function(){
	//we want to get the value of the input and then publish it in p
	//Since we already captured input, we can access its value via dot notation;
	//.value is a property of input fields
	//kung ano ung laman nung input
	const inputValue = wordInput.value;
	// console.log(inputValue);
	const p = document.getElementById('words');
	p.textContent = inputValue;
})

//Tomorrow:
//Registration/ Login
//Form Validation
//Fetch
//Code along for build divider

const btn = document.getElementById('byeBtn'); //byeBtn is the ID from the HTML File

// Step 2: 
// 2. Add an event listener
// addEventListener = method; takes 2 params (1. type of event, 2. Function)

btn.addEventListener('click', function(){
	// Step 4.
	//1. We need to capture the element we want to modify.
	//We want to change H1.
	//When we realized, we already targetted btn.
	//previousElementSibling
btn.previousElementSibling.textContent = "Bye World!";
	//we can do more here
	//next goal -- change the bg to red (bg-warning);
	//we realized, the first div is the one responsible for the background.
	//And we have the background because of the bootstrap class bg-info.
	//for us to change the background, we need to remove FIRST bg-info and then add bg-warning.
	//We realized, the div we want to target is the parent of btn.
	btn.parentElement.classList.remove('bg-info');
	btn.parentElement.classList.add('bg-danger');
	//change the color of the btn.
	//clue: btn-warning is the one responsible for the color

	btn.classList.remove('btn-warning');
	btn.classList.add('btn-info');

	setTimeout(function(){
		btn.previousElementSibling.textContent = "Hello World!";

	}, 2000)


});


